
const addButton = document.getElementById("button");
const input = document.getElementById("input");
const listContainer = document.getElementById("list-container");


function addClick() {
    if (input.value === '') {
        alert("You must write something!");
    }
    else {
        const li = document.createElement("li");
        const inputData = input.value;
        li.innerText = inputData;
        listContainer.appendChild(li);

        let span = document.createElement("span");
        span.innerHTML = "\u00d7"; // Unicode for multiplication sign (×)
        li.appendChild(span);
    }

    input.value = "";
    saveData()
}
function deleteItems(event) {

    if (event.target.tagName === "LI") {
        event.target.classList.toggle("checked");
        saveData()

    } else if (event.target.tagName === "SPAN") {
        event.target.parentElement.remove();
        saveData()

    }
}
function handleKeyPress(event) {
    if (event.key === "Enter") {
        addClick();
    }
}


addButton.addEventListener("click", addClick);
input.addEventListener("keydown", handleKeyPress);
listContainer.addEventListener("click", deleteItems);

function saveData() {
    localStorage.setItem("data", listContainer.innerHTML);
}
function showTask() {
    listContainer.innerHTML = localStorage.getItem("data");
}
showTask()